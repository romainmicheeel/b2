tp2 it4 - réseau
===
I. Exploration locale en solo

**1. Affichage d'informations sur la pile TCP/IP locale**

**nom et adresse MAC des interface WiFi et Ethernet :**
```
C:\Users\romai> Get-Netadapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Wi-Fi                     Intel(R) Wi-Fi 6 AX201 160MHz                21 Up           34-7D-F6-5A-89-99       400 Mbps

Ethernet                  Killer E2600 Gigabit Ethernet Contro...      10 Disconnected 08-97-98-AA-6D-F3          0 bps
```
ma carte Ethernet n'est pas connecté a internet, seule ma carte Wi-Fi à une **adresse ip** : 
```
PS C:\Users\romai> Get-NetIPConfiguration -InterfaceAlias Wi-Fi
[...]
IPv4Address          : 10.33.3.248
[...]
```

**Gateway**


```
[...]
IPv4DefaultGateway   : 10.33.3.253
[...]
```



*En graphique (GUI : Graphical User Interface)*


**IP, la MAC et la gateway pour l'interface WiFi de votre PC**
![](https://i.imgur.com/hdSX2OR.png)



**Questions**

La gateway du réseau d'YNOV permet aux appareils du réseau de se connecter à un réseau exterieur comme internet

2. Modifications des informations

**A. Modification d'adresse IP** (part 1)
![](https://i.imgur.com/qwDpfnp.png)


**B. Table ARP**
```
PS C:\Users\romai> arp -a
[...]
Interface : 10.33.3.248 --- 0x15
  Adresse Internet      Adresse physique      Type
  10.33.1.228           34-7d-f6-b2-82-cf     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```
Précédement on a pu découvrir l'adresse ip de la passerelle du réseau (10.33.3.253), on peut voir en face son adresse MAC : 00-12-00-40-4c-bf

🌞 Et si on remplissait un peu la table ?
```
[...]
Interface : 10.33.3.248 --- 0x15
  Adresse Internet      Adresse physique      Type
  10.33.0.100           e8-6f-38-6a-b6-ef     dynamique
  10.33.1.228           34-7d-f6-b2-82-cf     dynamique
  10.33.2.8             a4-5e-60-ed-0b-27     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```
j'ai ping ces adresses et je peux donc récuperer leurs adresses mac

| Adresses IP | Adresses MAC | 
| -------- | -------- | 
| 10.33.2.8   | a4-5e-60-ed-0b-27    |
| 10.33.2.88   | 68-3e-26-7c-c3-1a    |
| 10.33.0.100   |e8-6f-38-6a-b6-ef     |

**C. nmap**

exemple de commande pour le réseau YNOV  `nmap -sn -PE <ADRESSE_DU_RESEAU_CIBLE>`
pour trouver les hôtes actuellement sur le réseau

le réseau YNOV est 10.33.0.0/22
```
PS C:\Users\romai> nmap -sn -PE 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:40 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.11s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
[...]
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.010s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0030s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.3.248
Host is up.
Nmap done: 1024 IP addresses (97 hosts up) scanned in 38.53 seconds
[...]
```
par exemple nmap -sP 10.33.0.0/22 pour un ping scan du réseau YNOV

🌞Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre
```
MAC Address: 3E:02:9D:7D:30:8F (Unknown)
Nmap scan report for 10.33.3.239
Host is up (3.1s latency).
MAC Address: 38:F9:D3:AE:C3:A3 (Apple)
Nmap scan report for 10.33.3.249
Host is up (0.077s latency).
[...]
Nmap done: 1024 IP addresses (107 hosts up) scanned in 36.62 seconds
```
il y a donc des adresses dispo entre ces deux ci-dessus

en lançant un scan de ping sur le réseau YNOV,
voici **ma table ARP**
```
Interface : 10.33.3.248 --- 0x15
  Adresse Internet      Adresse physique      Type
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.31            fa-c5-6d-60-4b-4c     dynamique
  10.33.0.42            ce-a6-a5-8c-f3-7a     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.111           d2-41-f0-dc-6a-ed     dynamique
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.0.130           e4-5e-37-07-22-22     dynamique
  10.33.0.135           f8-5e-a0-06-40-d2     dynamique
  10.33.0.140           40-ec-99-8b-11-c2     dynamique
  10.33.0.143           f0-18-98-41-11-07     dynamique
  10.33.0.148           e6-aa-26-ee-23-b7     dynamique
  10.33.0.164           9a-75-ee-ef-e2-00     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.0.215           d8-f8-83-a6-59-2d     dynamique
  10.33.0.250           28-cd-c4-dd-db-73     dynamique
  10.33.1.63            b0-7d-64-b1-98-d3     dynamique
  10.33.1.70            30-57-14-94-de-fb     dynamique
  10.33.1.92            74-d8-3e-0d-06-b0     dynamique
  10.33.1.93            74-d8-3e-0d-06-b0     dynamique
  10.33.1.94            74-d8-3e-0d-06-b0     dynamique
  10.33.1.95            74-d8-3e-0d-06-b0     dynamique
  10.33.1.98            74-d8-3e-0d-06-b0     dynamique
  10.33.1.142           74-d8-3e-0d-06-b0     dynamique
  10.33.1.228           34-7d-f6-b2-82-cf     dynamique
  10.33.1.248           e8-84-a5-24-94-c9     dynamique
  10.33.2.48            80-91-33-9c-bf-0d     dynamique
  10.33.2.58            ac-bc-32-88-1f-e7     dynamique
  10.33.2.81            50-eb-71-d6-4e-8f     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.2.116           e2-be-82-4d-c5-8f     dynamique
  10.33.2.157           08-d2-3e-62-c0-dc     dynamique
  10.33.2.182           f4-4e-e3-c0-ed-29     dynamique
  10.33.2.196           08-71-90-87-b9-8c     dynamique
  10.33.2.201           d8-3b-bf-98-b1-6f     dynamique
  10.33.2.221           34-c9-3d-4d-a3-78     dynamique
  10.33.3.5             74-d8-3e-0d-06-b0     dynamique
  10.33.3.33            c8-58-c0-63-5a-92     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.99            b8-9a-2a-dc-0b-6d     dynamique
  10.33.3.131           3c-22-fb-84-d9-68     dynamique
  10.33.3.134           34-7d-f6-5a-20-da     dynamique
  10.33.3.139           34-cf-f6-37-2c-fb     dynamique
  10.33.3.148           bc-76-5e-d2-75-86     dynamique
  10.33.3.168           12-88-c1-71-b7-a0     dynamique
  10.33.3.187           96-fd-87-13-4b-ee     dynamique
  10.33.3.189           c2-6f-43-3d-c7-fa     dynamique
  10.33.3.201           82-74-57-ee-b9-59     dynamique
  10.33.3.214           6c-40-08-bc-78-92     dynamique
  10.33.3.225           34-2e-b7-49-5a-cf     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
*j'ai donc communiqué avec toutes ces adresses ip*

**D. Modification d'adresse IP (part 2)**

🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap

![](https://i.imgur.com/ZAU2g3P.png)



après un ping scan sur le réseau YNOV, voici **la commande nmap et son résultat**
```
PS C:\Users\romai> nmap -sn 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 11:32 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.0050s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
Host is up (0.31s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
Nmap scan report for 10.33.0.19
Host is up (0.80s latency).
MAC Address: D4:A3:3D:C8:CA:67 (Apple)
Nmap scan report for 10.33.0.21
Host is up (0.0050s latency).
[...]
MAC Address: 5C:3A:45:06:7A:5F (Chongqing Fugui Electronics)
Nmap scan report for 10.33.3.252
Host is up (0.0020s latency).
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.0080s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0050s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.3.248
Host is up.
Nmap done: 1024 IP addresses (110 hosts up) scanned in 41.34 seconds
```
j'utilise toujours la gateway d'YNOV pour avoir accès à d'autres réseaux, ma passerelle est bien définie, et j'ai un accès internet
```
PS C:\Users\romai> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 18ms, Moyenne = 17ms
    
    [...]
    
    Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::d9dc:8dd5:a8c2:e7e0%21
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.248
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

II. Exploration locale en duo
===

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

### Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

choisissez une IP qui commence par "192.168"
utilisez un /30 (que deux IP disponibles)


vérifiez à l'aide de commandes que vos changements ont pris effet
utilisez ping pour tester la connectivité entre les deux machines
affichez et consultez votre table ARP
```
PS C:\Users\romai> ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
[...]
Interface : 192.168.0.2 --- 0xa
  Adresse Internet      Adresse physique      Type
  169.254.72.16         d4-5d-64-5a-43-5c     dynamique
  192.168.0.1           d4-5d-64-5a-43-5c     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  [...]
```

### Utilisation d'un des deux comme gateway

🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

Mon PC sert donc de routeur au pc qui est branché en Ethernet
```
C:\Users\berti>tracert google.com

Détermination de l’itinéraire vers google.com [142.250.178.142]
avec un maximum de 30 sauts :

  1     2 ms     1 ms     1 ms  LAPTOP-0PRP7B52 [192.168.0.2]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms     6 ms    14 ms  192.168.1.1
  4    38 ms    38 ms    34 ms  10.125.51.5
  5    38 ms    44 ms    41 ms  10.125.127.82
  6    46 ms    45 ms    31 ms  10.151.14.50
  7    38 ms    39 ms    44 ms  89.89.101.56
  8    46 ms    38 ms    38 ms  62.34.2.169
  9    40 ms    28 ms    57 ms  be5.cbr01-ntr.net.bbox.fr [212.194.171.137]
 10     *        *       43 ms  62.34.2.56
 11    37 ms    65 ms    41 ms  72.14.204.68
 12    38 ms    28 ms    47 ms  108.170.245.1
 13    50 ms    40 ms    38 ms  142.251.64.131
 14    44 ms    37 ms    45 ms  par21s22-in-f14.1e100.net [142.250.178.142]

Itinéraire déterminé.
```
*Sur le pc branché*

### 5. Petit chat privé

🌞 sur le PC serveur avec par exemple l'IP 192.168.0.1
```
C:\Users\romai\AppData\Local\Temp\netcat-1.11>nc.exe -l -p 8888
yoooooo
df

fds

f

fdfgg
g
g
stop

g
g
g
g
g
g
g
g
g
gros con
g
```

🌞 sur le PC client avec par exemple l'IP 192.168.0.2

```
C:\Users\berti\OneDrive\Documents\Cours YNOV\B2\TP1-réseau\netcat>nc.exe 192.168.0.2 8888
hh
hooo
```

🌞 pour aller un peu plus loin
```
C:\Users\romai\AppData\Local\Temp\netcat-1.11>nc.exe -l -p 8888 192.168.0.1
hh
hooo
^C
```
*on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur 127.0.0.1*
```
C:\Users\romai\AppData\Local\Temp\netcat-1.11>nc.exe 127.0.0.1 8888
tg
dacc
```

### 6. Firewall

🌞 Autoriser les ping

![](https://i.imgur.com/nf2INDZ.png)

🌞 Autoriser nc sur un port spécifique

![](https://i.imgur.com/l3WDome.png)

```
C:\Users\romai\AppData\Local\Temp\netcat-1.11>nc.exe -l -p 4560
tg
dacc
```



### III. Manipulations d'autres outils/protocoles côté client

#### 1. DHCP

🌞Exploration du DHCP, depuis votre PC

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
    [...]
   Serveur DHCP . . . . . . . . . . . . . : 192.168.0.254
    [...]
```

#### 2. DNS

🌞l'adresse IP du serveur DNS que connaît mon ordinateur
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
  [...]
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.0.254
[...]
```


🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

```
PS C:\Users\romai> nslookup google.com
Serveur :   one.one.one.one
Address:  1.1.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80b::200e
          142.250.179.110
```
```
PS C:\Users\romai> nslookup ynov.com
Serveur :   one.one.one.one
Address:  1.1.1.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
**Reverse lookup**
```
PS C:\Users\romai> nslookup 78.74.21.21
Serveur :   one.one.one.one
Address:  1.1.1.1

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

PS C:\Users\romai>
>> nslookup 92.146.54.88
Serveur :   one.one.one.one
Address:  1.1.1.1

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
On obtient le nom de domaine attribué a ces adresses ip




### IV. Wireshark

* le ping 

![](https://i.imgur.com/wsT2fik.png)

* le netcat

![](https://i.imgur.com/Nm625ec.png)

* le dns

![](https://i.imgur.com/u9q2w20.png)


